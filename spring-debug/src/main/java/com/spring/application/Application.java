/*
 * Copyright 2002-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.spring.application;


import com.spring.bean.HelloWorld;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

final class Application {
	private Application(){}

	public static void main(String[] args) {
		//1. 创建 Spring 的 IOC 容器
		AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		//2. 从 IOC 容器中获取 bean 的实例
		HelloWorld helloWorld = ctx.getBean(HelloWorld.class);
		// 根据类型来获取 bean 的实例: 要求在  IOC 容器中只有一个与之类型匹配的 bean, 若有多个则会抛出异常.
		// 一般情况下, 该方法可用, 因为一般情况下, 在一个 IOC 容器中一个类型对应的 bean 也只有一个.
		// HelloWorld helloWorld1 = ctx.getBean(HelloWorld.class);
		//3. 使用 bean
		helloWorld.hello();
	}
}
